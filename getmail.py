server_name = "smtp.gmail.com"
port = 587
sender_username = "tnpnotifier@gmail.com"
sender_password = "tnpnotify.exe"
toaddrs  = ['masterkapilkumar@gmail.com','korkudeepak@gmail.com']

from bs4 import BeautifulSoup
import requests
import smtplib
import time
while True:
	url = "http://tnp.iitd.ac.in/notices/training/notify.php"

	old_data = ""
	try:
		history = open("history", 'r')
		old_data = history.read()
		history.close()
	except IOError:
		print("File does not exist!")


	headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'}

	response = requests.get(url, headers=headers)
	soup = BeautifulSoup(response.content, 'html.parser')

	new_data = ""
	for i in range(1,7):
		notifications = soup.find_all("div", {"id": str(i)})[0]
		for row in notifications.find_all("tr"):
			for data in row.find_all("td"):
				if(data.b):
					new_data+=(str(data.b.contents[0])+"\n")
			new_data+="\n"

	if(old_data==new_data):
		print("No new notification")
	else:
		print("New notifications!")
		print("Sending...")

		fromaddr = 'tnpinfo@gmail.com'
		msg = "\r\n".join([
			"From: "+ fromaddr,
			"To: " + str(toaddrs),
			"Subject: Tnp Notification",
			"",
			new_data
			])

		server = smtplib.SMTP(server_name, port)
		server.ehlo()
		server.starttls()
		server.login(sender_username, sender_password)
		server.sendmail(fromaddr, toaddrs, msg)
		server.quit()
		print("Sent!")
		history = open("history", 'w+')
		history.write(new_data)
		history.close()
	time.sleep(10)
